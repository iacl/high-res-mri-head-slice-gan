# A deep generative prior for high-resolution isotropic MR head slices

This respository shows basic usage of the provided pre-trained weights via a sample script `generate.py`. This is a simplified version of existing code in the official StyleGAN3 repository, and demonstrates how to generate a random image as well as how to perform style-mixing, if desired. As mentioned in the paper, empirically "good" slices are those with histogram standard deviations > 0.125; this function can of course be modified to suit your needs.

## Installation

### `git-lfs` for model weights
First, clone the repo. We use `git-lfs` to store/retrieve the model weights. If you don't have `git-lfs` installed on your system, only links to the weights will be cloned rather than the actual weights themselves. Therefore, you must first install `git-lfs` (and run `git lfs install`) before cloning the repository.

There are multiple options to install `git-lfs`:
1. Using your system's package manager; e.g., `apt install git-lfs` or `brew install git-lfs`
2. Install `git-lfs` from source: (when testing on WSL, I needed to use this option)

```
curl -Lo git-lfs-linux-amd64-v3.2.0.tar.gz https://github.com/git-lfs/git-lfs/releases/download/v3.2.0/git-lfs-linux-amd64-v3.2.0.tar.gz
tar -xzf git-lfs-linux-amd64-v3.2.0.tar.gz
cd git-lfs-3.2.0
PREFIX=${HOME}/.local ./install.sh
```

If you choose this latter option, ensure that `git-lfs` is on your `PATH`. For example, `export PATH=${HOME}/.local/bin:${PATH}`.


### `conda` for required Python packages
We use the same Python libraries as StyleGAN3: https://github.com/NVlabs/stylegan3

The same installation steps are recommended here:
- `conda env create -f environment.yml`
- `conda activate stylegan3`

As mentioned in the original repo, there are custom PyTorch extensions implemented by the NVIDIA team. Please refer to their repo for further details if necessary.

## Usage
The true intent of this repository is to provide open access to pre-trained weights. To make use of them, `generate.py` serves as a guiding template.

For example, to generate images, run:
```
python generate.py --weight-fpath /PATH/TO/WEIGHTS/FILE_T1.pkl --gpu_id GPU_ID --out-dir /PATH/TO/OUT/DIRECTORY/
```

T1-w images will be generated from the T1 `.pkl` file, and T2-w images generated from the T2 `.pkl` file. This saves results as `.npy` arrays to disk. The script `generate.py` can be modified to generate many images (add a loop) or style-mix (or disable) as desired.

## Citation
```
@inproceedings{
    Remedios2023-MRIGenerativePrior,
    title={A deep generative prior for high-resolution isotropic MR head slices},
    author={Remedios, Samuel W and Dewey, Blake E and Carass, Aaron and Pham, Dzung L and Prince, Jerry L},
    booktitle={Medical Imaging 2023: Image Processing},
    year={2023},
    organization={SPIE}
}

```
