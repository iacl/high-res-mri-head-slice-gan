"""
This script is a simple example to show how to load weights and
generate images, including style transfer. 

Many functions here are directly lifted or slightly simplified from
https://github.com/NVlabs/stylegan3/

Additionally, in order to load the model, the directories
`dnnlib/` and `torch_utils/` are required, also directly cloned from
https://github.com/NVlabs/stylegan3/
"""
import argparse
import numpy as np
import pickle
import torch
from pathlib import Path


def load_network_pkl(f):
    data = pickle.load(f)

    # Add missing fields.
    if "training_set_kwargs" not in data:
        data["training_set_kwargs"] = None
    if "augment_pipe" not in data:
        data["augment_pipe"] = None

    # Validate contents.
    assert isinstance(data["G"], torch.nn.Module)
    assert isinstance(data["D"], torch.nn.Module)
    assert isinstance(data["G_ema"], torch.nn.Module)
    assert isinstance(data["training_set_kwargs"], (dict, type(None)))
    assert isinstance(data["augment_pipe"], (torch.nn.Module, type(None)))

    return data


def make_transform(translate, angle):
    m = np.eye(3)
    s = np.sin(angle / 360.0 * np.pi * 2)
    c = np.cos(angle / 360.0 * np.pi * 2)
    m[0][0] = c
    m[0][1] = s
    m[0][2] = translate[0]
    m[1][0] = -s
    m[1][1] = c
    m[1][2] = translate[1]
    return m

def is_good_slice(x):
    '''
    Empirically, "good" slices were found to have a std
    of 0.125 or greater. This avoids the "empty" slice.
    '''
    return x.std() > 0.125


def gen_img(
    G,
    z1,
    z2=None,
    mix_offset=0,
    c=None,
    transform=None,
    truncation_psi=1,
    truncation_cutoff=None,
    update_emas=False,
    **synthesis_kwargs,
):
    w1 = G.mapping(
        z1,
        c,
        truncation_psi=truncation_psi,
        truncation_cutoff=truncation_cutoff,
        update_emas=update_emas,
    )

    if z2 is not None:
        w2 = G.mapping(
            z2,
            c,
            truncation_psi=truncation_psi,
            truncation_cutoff=truncation_cutoff,
            update_emas=update_emas,
        )
        ws = torch.cat(
            [
                w1[:, : w1.shape[1] // 2 - mix_offset],
                w2[:, w2.shape[1] // 2 - mix_offset :],
            ],
            axis=1,
        )
    else:
        ws = w1

    if transform is None:
        transform = make_transform((0, 0), 0)
        transform = np.linalg.inv(transform)

    if hasattr(G.synthesis, "input"):
        G.synthesis.input.transform.copy_(torch.from_numpy(transform))

    img = G.synthesis(ws, update_emas=update_emas, **synthesis_kwargs)
    img = img.permute(0, 2, 3, 1).detach().cpu().numpy().squeeze()

    return img


if __name__ == "__main__":
    # ===== Arguments ===== #
    parser = argparse.ArgumentParser()
    parser.add_argument("--weight-fpath", type=str, required=True)
    parser.add_argument("--gpu-id", type=int, default=-1)
    parser.add_argument("--out-dir", type=str, default=".")

    args = parser.parse_args()

    if args.gpu_id == -1:
        device = torch.device("cpu")
    else:
        device = torch.device(f"cuda:{args.gpu_id}")

    # ===== Load network ===== #
    print(f"Loading network to {device}...")
    with open(args.weight_fpath, "rb") as f:
        G = load_network_pkl(f)["G_ema"].to(device)

    # ===== Generate image ===== #
    print(f"Generating images...")
    # First, just a random image
    z1 = torch.randn(size=(1, G.z_dim), device=device)
    img1 = gen_img(G, z1)
    
    # Re-roll until a nice slice is found
    while not is_good_slice(img1):
        z1 = torch.randn(size=(1, G.z_dim), device=device)
        img1 = gen_img(G, z1)

    # Second, a style mix between a nearby latent code
    n = torch.randn(size=z1.shape, device=device) / 1.5
    z2 = z1 + n
    img2 = gen_img(G, z2)

    img_mix = gen_img(G, z1, z2, mix_offset=-1)

    # ===== Write to disk ===== #
    out_dir = Path(args.out_dir)
    if not out_dir.exists():
        out_dir.mkdir(parents=True)

    print(f"Saving images to {out_dir}...")
    np.save(out_dir / "img1.npy", img1)
    np.save(out_dir / "img2.npy", img2)
    np.save(out_dir / "img_mix.npy", img_mix)
